package by.resliv.travel.bot.bot.entity;

import lombok.*;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class City {
    private Long id;
    private String name;
    private String description;
}
