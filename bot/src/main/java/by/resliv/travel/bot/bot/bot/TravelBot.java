package by.resliv.travel.bot.bot.bot;

import by.resliv.travel.bot.bot.entity.City;
import by.resliv.travel.bot.bot.service.BotService;
import by.resliv.travel.bot.bot.service.exception.CityNotFoundException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

@Component
@Log4j2
public class TravelBot extends TelegramLongPollingBot {

    private static final String UNKNOWN_CITY_MSG = "Хмм...\nО таком городе я ничего не слышал.";
    private static final String START_MSG = "Привет, напиши мне название города, а я тебе расскажу что-нибудь о нем.";
    private static final String START_KEY = "/start";

    @Value("${telegram.bot.name}")
    private String name;

    @Value("${telegram.bot.token}")
    private String token;

    @Autowired
    private BotService botService;

    @Override
    public void onUpdateReceived(Update update) {
        Message message = update.getMessage();
        if (message != null && message.hasText()) {
            String stringMsg = message.getText();
            log.info(message.getChatId() + ": " + stringMsg);
            if (stringMsg.equalsIgnoreCase(START_KEY)) {
                sendMessage(message, START_MSG);
            } else {
                try {
                    City city = botService.findByName(stringMsg);
                    sendMessage(message, city.getDescription());
                } catch (CityNotFoundException e) {
                    log.info(e.getMessage());
                    sendMessage(message, UNKNOWN_CITY_MSG);
                }
            }

        }
    }


    private void sendMessage(Message message, String text) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);
        sendMessage.setChatId(message.getChatId().toString());
        sendMessage.setReplyToMessageId(message.getMessageId());
        sendMessage.setText(text);
        try {
            execute(sendMessage);
        } catch (TelegramApiException e) {
            log.error("Cannot send message", e);
        }

    }

    @Override
    public String getBotUsername() {
        return name;
    }

    @Override
    public String getBotToken() {
        return token;
    }

}
