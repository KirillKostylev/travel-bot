package by.resliv.travel.bot.bot.service;

import by.resliv.travel.bot.bot.entity.City;
import by.resliv.travel.bot.bot.service.exception.CityNotFoundException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@Service
public class BotService {

    @Value("${backend.server.url}")
    private String backendUrl;


    public City findByName(String cityName) throws CityNotFoundException {
        RestTemplate restTemplate = new RestTemplate();
        try {
            City city = restTemplate.getForObject(backendUrl + "/name?cityName="+cityName, City.class);
            return city;
        }catch (RestClientException e){
            throw new CityNotFoundException(e.getMessage());
        }
    }
}
