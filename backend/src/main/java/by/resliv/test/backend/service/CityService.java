package by.resliv.test.backend.service;

import by.resliv.test.backend.entity.City;
import by.resliv.test.backend.service.exception.CityNotFoundException;

import java.util.List;

public interface CityService extends Service<City, Long> {
    City findCityByName(String cityName) throws CityNotFoundException;

    List<City> findAllCities();
}
