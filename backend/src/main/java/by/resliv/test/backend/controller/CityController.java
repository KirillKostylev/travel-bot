package by.resliv.test.backend.controller;

import by.resliv.test.backend.entity.City;
import by.resliv.test.backend.service.CityService;
import by.resliv.test.backend.service.exception.CityAlreadyExistException;
import by.resliv.test.backend.service.exception.CityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/cities")
public class CityController {

    @Autowired
    private CityService cityService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<City> create(@RequestBody City city) throws CityAlreadyExistException {
        City newCity = cityService.save(city);
        return ResponseEntity.ok(newCity);
    }

    @PutMapping
    public ResponseEntity<City> update(@RequestBody City city) throws CityAlreadyExistException {
        City newCity = cityService.save(city);
        return ResponseEntity.ok(newCity);
    }

    @DeleteMapping
    public void deleteByName(@RequestBody City city) {
        cityService.delete(city);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable("id") Long id) {
        cityService.deleteById(id);
    }

    @GetMapping()
    public ResponseEntity<List<City>> findByName(@RequestParam(value = "cityName", required = false) String cityName)
            throws CityNotFoundException {
        if (cityName == null) {
            return ResponseEntity.ok(cityService.findAllCities());
        }
        List<City> cities = new ArrayList<>();
        cities.add(cityService.findCityByName(cityName));
        return ResponseEntity.ok(cities);
    }

    @GetMapping("/{id}")
    public ResponseEntity<City> findById(@PathVariable("id") Long id) throws CityNotFoundException {
        return ResponseEntity.ok(cityService.findById(id));
    }

    @GetMapping("/name")
    public ResponseEntity<City> findByName2(@RequestParam(value = "cityName") String cityName) throws CityNotFoundException {
        return ResponseEntity.ok(cityService.findCityByName(cityName));
    }
}
