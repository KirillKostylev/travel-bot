package by.resliv.test.backend.repository;

import by.resliv.test.backend.entity.City;
import org.springframework.data.repository.CrudRepository;


public interface CityRepository extends CrudRepository<City, Long> {
    City findCityByName(String cityName);
}

