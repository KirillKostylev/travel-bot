package by.resliv.test.backend.controller;

import by.resliv.test.backend.service.exception.CityAlreadyExistException;
import by.resliv.test.backend.service.exception.CityNotFoundException;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(CityNotFoundException.class)
    protected ResponseEntity<OutputException> handleCityNotFoundException(CityNotFoundException e) {
        return new ResponseEntity<>(new OutputException(e.getMessage()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(CityAlreadyExistException.class)
    protected ResponseEntity<OutputException> handleCityAlreadyExistException(CityAlreadyExistException e) {
        return new ResponseEntity<>(new OutputException(e.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @Data
    @AllArgsConstructor
    private static class OutputException {
        private String message;
    }
}
