package by.resliv.test.backend.service;

import by.resliv.test.backend.service.exception.CityAlreadyExistException;
import by.resliv.test.backend.service.exception.CityNotFoundException;

public interface Service<ENTITY, KEY> {
    ENTITY save(ENTITY entity) throws CityAlreadyExistException;

    ENTITY findById(KEY id) throws CityNotFoundException;

    void deleteById(KEY id);

    void delete(ENTITY entity);
}
