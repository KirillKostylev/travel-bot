package by.resliv.test.backend.service.exception;


public class CityNotFoundException extends ServiceException {

    public CityNotFoundException(String message) {
        super(message);
    }

    public CityNotFoundException(String message, Throwable root) {
        super(message, root);
    }

}
