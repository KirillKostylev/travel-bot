package by.resliv.test.backend.service.exception;

public class CityAlreadyExistException extends ServiceException {
    public CityAlreadyExistException(String message) {
        super(message);
    }

    public CityAlreadyExistException(String message, Throwable cause) {
        super(message, cause);
    }
}
