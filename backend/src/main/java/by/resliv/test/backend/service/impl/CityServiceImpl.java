package by.resliv.test.backend.service.impl;

import by.resliv.test.backend.entity.City;
import by.resliv.test.backend.repository.CityRepository;
import by.resliv.test.backend.service.CityService;
import by.resliv.test.backend.service.exception.CityAlreadyExistException;
import by.resliv.test.backend.service.exception.CityNotFoundException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Log4j2
public class CityServiceImpl implements CityService {

    @Autowired
    private CityRepository cityRepository;


    @Override
    public City save(City city) throws CityAlreadyExistException {
        try {
            City newCity = cityRepository.save(city);
            return newCity;
        } catch (Exception e) {
            String errorMsg = "City with name=" + city.getName() + " already exist.";
            log.error(errorMsg);
            throw new CityAlreadyExistException(errorMsg);
        }
    }

    @Override
    public City findById(Long id) throws CityNotFoundException {
        Optional<City> byId = cityRepository.findById(id);
        if (!byId.isPresent()) {
            String errorMsg = "City with id=" + id + " not found";
            throw new CityNotFoundException(errorMsg);
        }
        return byId.get();
    }

    @Override
    public void deleteById(Long id) {
        cityRepository.deleteById(id);
    }

    @Override
    public void delete(City city) {
        cityRepository.delete(city);
    }

    @Override
    public City findCityByName(String cityName) throws CityNotFoundException {
        City cityByName = cityRepository.findCityByName(cityName);
        if (cityByName == null) {
            String errorMsg = "City with name=" + cityName + " not found";
            throw new CityNotFoundException(errorMsg);
        }
        return cityByName;
    }

    @Override
    public List<City> findAllCities() {
        return (List<City>) cityRepository.findAll();
    }
}
